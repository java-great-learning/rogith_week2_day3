package com.week2.day3;
import java.util.HashMap;

public class LabQues2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashMap<String, Integer> str = new HashMap<String, Integer>();
		
		str.put("Rogith", 100);
		str.put("Shalini", 40);
		str.put("Arun", 90);
		str.put("Harish", 80);
		
		System.out.println("Map Size is "+str.size());
		
		System.out.println(str);
		
		if(str.containsKey("Rogith")) {
			
			System.out.println("Value of the Key is "+str.get("Rogith"));
		}
	}

}
