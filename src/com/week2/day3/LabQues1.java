package com.week2.day3;

import java.util.Comparator;
import java.util.PriorityQueue;

public class LabQues1 {
	public static void main(String[] args) {
	Comparator<String> str = new Comparator<String>() {
		@Override
		public int compare(String s1, String s2) {
			return s1.length() - s2.length();
		}
	};
	
	PriorityQueue<String> pque = new PriorityQueue<>(str);
	pque.add("India");
	pque.add("China");
	pque.add("Pakistan");
	pque.add("Uber");
	pque.add("Zomato");
	pque.add("Swiggy");
	
	while(!pque.isEmpty())
	{
		System.out.println(pque.remove());
	}
	
}
}