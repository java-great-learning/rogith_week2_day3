package com.week2.day3;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MapDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map<String, Integer> demo = new HashMap<String, Integer>();
		demo.put("K1",106);
		demo.put("K2",102);
		demo.put("K3",102);
		demo.put("K4",103);
		demo.put("K5",150);
		System.out.println(demo);
		System.out.println(demo.putIfAbsent("K1", 1));
		System.out.println(demo.keySet());
		System.out.println(demo.values());
		System.out.println(demo.size());
		
		for(String key: demo.keySet()) {
			System.out.println(demo.keySet()+""+demo.values());
		}
	}

}
