package com.week2.day3;
import java.util.Comparator;
import java.util.List;

public class MyGenerics<T> {
	/*
	 * T-Type, V-Value, K-Key, E-Elements
	 */
	
	T obj;
	public MyGenerics(T obj) {
		this.obj = obj;
		}
	
	public T getObj()
	{
		return obj;
	}
	public void setObj()
	{
		this.obj =  obj;
	}
}
