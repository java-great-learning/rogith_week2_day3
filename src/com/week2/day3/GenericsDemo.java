package com.week2.day3;

import java.util.ArrayList;
import java.util.List;

public class GenericsDemo {
	public static void m1(List list)
	{
		list.add("Shalini");
		System.out.println(list.size());
	}
	public static <T> void calculate(T number)
	{
		if(number instanceof Number)
		{
			System.out.println((Integer)number);
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<Integer> list = new ArrayList<Integer>();
		
		
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(50);
		System.out.println(list.size());
		m1(list);
		System.out.println(list);
		MyGenerics<Integer> g1 = new MyGenerics<Integer>(1);
		System.out.println(g1.getObj());

		calculate(10);
		calculate("Shalini");
	}

}
