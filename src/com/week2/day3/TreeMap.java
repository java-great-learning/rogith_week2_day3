package com.week2.day3;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

public class TreeMap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> list = new ArrayList<String>();
		
		list.add("Shalini");
		list.add("Rogith");
		list.add("Shristi");
		list.add("Deepak");
		list.add("Sunil");
		list.add("Shalini");
		list.add("Sunil");
		list.add("Deepak");
		list.add("Shalini");
		list.add("Shristi");
		list.add("Rogith");
		list.add("Deepak");
		list.add("Arun");
		list.add("Gowtham");
		list.add("Arun");
		list.add("Shalini");
		
        HashMap<String,Integer> count = new HashMap<String, Integer>();
		
		for(String name : list)
		{
			if(count.containsKey(name))
			{
				count.put(name, count.get(name)+1);
			}
			else
			{
				count.put(name, 1);
			}
		}
		
		for(String name: count.keySet())
		{
			System.out.println(name+":" +count.get(name));
		}
	}
	
}
